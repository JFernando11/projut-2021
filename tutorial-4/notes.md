### Lazy Instantiation

Lazy Instatiation adalah suatu teknik yang dapat dilakukan untuk menghemat resource dan juga waktu eksekusi.
Hal ini terjadi karena kita menginisialisasi instance dengan nilai null dan baru mengisi nilai apabila terdapat request.

#### Kelebihan:
- Execution Time lebih cepat
- Resource/Memory yang diperlukan lebih hemat

#### Kekurangan:
- Bisa terjadi keadaan dimana nilai instance masih null dan terdapat 2 orang yang melakukan request secara bersamaan.
Apabila hal ini terjadi, maka new instance dapat terbuat 2 kali sekaligus sehingga merusak Singleton.
  
### Eager Instatiation

Eager Instatiation adalah suatu teknik yang dapat digunakan untuk mencegah terjadinya pembuatan 2 instance baru secara bersamaan.

#### Kelebihan:
- Lebih minim bug/kerusakan
- Properti Singleton tetap terjaga

#### Kelemahan:
- Execution Time lebih lambat karena selalu melakukan inisialisasi di awal.