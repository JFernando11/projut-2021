package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;

public class LiyuanSobaFactory extends IngridientFactory {
    @Override
    public Meat createMeat() {
        return new Beef();
    }

    @Override
    public Flavor createFlavor() {
        return new Sweet();
    }

    @Override
    public Noodle createNoodle() {
        return new Soba();
    }

    @Override
    public Topping createTopping() {
        return new Mushroom();
    }
}