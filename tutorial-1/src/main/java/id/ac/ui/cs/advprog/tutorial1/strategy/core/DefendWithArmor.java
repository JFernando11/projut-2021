package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public String getType() {
        return "armor";
    }

    public String defend() {
        return "Defend with armor";
    }
}
