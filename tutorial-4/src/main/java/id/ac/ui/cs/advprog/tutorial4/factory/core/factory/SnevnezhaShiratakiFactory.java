package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;

public class SnevnezhaShiratakiFactory extends IngridientFactory {
    @Override
    public Meat createMeat() {
        return new Fish();
    }

    @Override
    public Flavor createFlavor() {
        return new Umami();
    }

    @Override
    public Noodle createNoodle() {
        return new Shirataki();
    }

    @Override
    public Topping createTopping() {
        return new Flower();
    }
}