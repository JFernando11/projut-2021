package id.ac.ui.cs.tutorial0.service;

public interface AdventurerPowerClassifierService {
    public String classifyPower(int power);
}
