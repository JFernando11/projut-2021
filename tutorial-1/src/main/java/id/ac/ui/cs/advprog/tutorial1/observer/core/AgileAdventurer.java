package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    public void update() {
        if (guild.getQuestType().startsWith("D"))
            getQuests().add(guild.getQuest());
        if (guild.getQuestType().startsWith("R"))
            getQuests().add(guild.getQuest());
    }
}
