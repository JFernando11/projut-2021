package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    public void update() {
        if (guild.getQuestType().startsWith("D"))
            getQuests().add(guild.getQuest());
        if (guild.getQuestType().startsWith("E"))
            getQuests().add(guild.getQuest());
    }
}
