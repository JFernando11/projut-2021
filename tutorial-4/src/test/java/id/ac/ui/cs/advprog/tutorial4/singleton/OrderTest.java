package id.ac.ui.cs.advprog.tutorial4.singleton;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderTest {
    private Class<?> orderDrinkClass;
    private Class<?> orderFoodClass;
    private OrderDrink orderDrink;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName(OrderDrink.class.getName());
        orderFoodClass = Class.forName(OrderFood.class.getName());
        orderDrink = OrderDrink.getInstance();
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testGetDrinkInstanceWillNotReturnNull() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        assertNotNull(orderDrink);
    }

    @Test
    public void testGetFoodInstanceWillNotReturnNull() {
        OrderFood orderFood = OrderFood.getInstance();
        assertNotNull(orderFood);
    }

    @Test
    public void testOrderADrink(){
        orderDrink.setDrink("Fresh Sparkling Water");
        assertEquals("Fresh Sparkling Water", orderDrink.getDrink());
    }

    @Test
    public void testOrderAFood(){
        orderFood.setFood("Javanese Salad with Peanut Sauce");
        assertEquals("Javanese Salad with Peanut Sauce", orderFood.getFood());
    }

    @Test
    public void testOrderADrinkWillNotReturnNull(){
        orderDrink.setDrink("Fresh Sparkling Water");
        assertNotNull(orderDrink.toString());
    }

    @Test
    public void testOrderAFoodWillNotReturnNull(){
        orderFood.setFood("Javanese Salad with Peanut Sauce");
        assertNotNull(orderFood.toString());
    }
}