package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    public String getType() {
        return "magic";
    }

    public String attack() {
        return "Attack with magic";
    }
}
