package id.ac.ui.cs.advprog.tutorial4.singleton.Controller;

import id.ac.ui.cs.advprog.tutorial4.singleton.controller.SingletonController;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = SingletonController.class)
public class SingletonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Test
    public void whenOrderDrinkIsMadeShouldCallOrderADrink() throws Exception {
        mockMvc.perform(post("/singleton/order-a-drink")
                .param("drink", "Fresh Sparkling Water"))
                .andExpect(handler().methodName("orderADrink"));

        verify(orderService, times(1)).orderADrink("Fresh Sparkling Water");
    }

    @Test
    public void whenOrderFoodIsMadeShouldCallOrderAFood() throws Exception {
        mockMvc.perform(post("/singleton/order-a-food")
                .param("food", "Javanese Salad with Peanut Sauce"))
                .andExpect(handler().methodName("orderAFood"));

        verify(orderService, times(1)).orderAFood("Javanese Salad with Peanut Sauce");
    }
}