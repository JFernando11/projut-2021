package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isStrong;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isStrong = false;
    }

    @Override
    public String normalAttack() {
        isStrong = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (isStrong) {
            return "Magic power not enough for large spell";
        }
        else {
            isStrong = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
