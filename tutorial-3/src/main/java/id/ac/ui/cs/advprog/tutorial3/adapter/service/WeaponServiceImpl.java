package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeapons = weaponRepository.findAll();
        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                allWeapons.add(new BowAdapter(bow));
            }
        }
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                allWeapons.add(new SpellbookAdapter(spellbook));
            }
        }
        return allWeapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        String log;
        if (weaponRepository.findByAlias(weaponName) != null) {
            weapon = weaponRepository.findByAlias(weaponName);
        }
        else if (bowRepository.findByAlias(weaponName) != null) {
            weapon = new BowAdapter(bowRepository.findByAlias(weaponName));
        }
        else {
            weapon = new SpellbookAdapter(spellbookRepository.findByAlias(weaponName));
        }
        weaponRepository.save(weapon);
        if (attackType == 0) {
            log = weapon.getHolderName() + " attacked with " + weapon.getName() + " (normal attack): " + weapon.normalAttack();
        }
        else {
            log = weapon.getHolderName() + " attacked with " + weapon.getName() + " (charged attack): " + weapon.chargedAttack();
        }
        logRepository.addLog(log);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
