package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

// Kelas ini menggeser setiap karakter sebanyak key secara siklik

public class BirthDateTransformation {
    private int key;

    public BirthDateTransformation(){
        this.key = 11072001; // My Birth Date: 11/07/2001
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        int n = text.length();
        int codexSize = codex.getCharSize();
        int delta = key % codexSize;
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newCharIdx = charIdx + delta * selector;
            newCharIdx = newCharIdx < 0 ? codexSize + newCharIdx : newCharIdx % codexSize;
            res[i] = codex.getChar(newCharIdx);
        }

        return new Spell(new String(res), codex);
    }
}
