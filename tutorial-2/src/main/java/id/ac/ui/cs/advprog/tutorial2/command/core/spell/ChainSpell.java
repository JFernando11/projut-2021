package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spellRepository;

    public ChainSpell(ArrayList<Spell> spellRepository) {
        this.spellRepository = spellRepository;
    }

    @Override
    public void cast() {
        for (Spell s : spellRepository)
            s.cast();
    }

    @Override
    public void undo() {
        int sz = spellRepository.size();
        for (int i = sz - 1; i >= 0; i--)
            spellRepository.get(i).undo();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
