package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public String getType() {
        return "barrier";
    }

    public String defend() {
        return "Defend with barrier";
    }
}
